# Evaluator

A LookingGlass-like tool for Gtk applications written in JavaScript.

![Evaluator as an inspector page](./examples/inspector.png)

![Evaluator as a window widget](./examples/widget.png)

## Features

* Code evaluation
* Completion
* Object inspection

## Integration

It is provided as an ES module but it is possible to import it in a "legacy" script.

```import Evaluator from './path…/evaluator.js';```  
or ```import('./path…/evaluator.js').then(({ default: Evaluator }) => …);```

The `Evaluator` class has a `context` property, a JS object that assumes the entry point role (alongside `globalThis`) and will be bound to the evaluated code as `this`.

```let evaluator = new Evaluator({ context: { application, … } });```

It is a widget:

```window.set_child(evaluator);```

Since the natural place of such a tool is the Gtk Inspector, there is also an integration helper (static method):

```Evaluator.addToInspector(evaluator);```

See the [examples](./examples/).

## Usage

### Input

* `Ctrl + B` and `Ctrl + N` to browse the history.
* `Up` and `Down` to browse the results when the entry is empty.
* `r(i)` to retrieve a result.
* `Shift + Return` to start a new line, or `Return` if `Enter activates the entry` is unchecked.
* `Tab` to get completion and/or suggestions.
* `Attached constantes` to manage recurrent constantes.
* `Evaluate the code as a function` skips `eval()` but makes the `return` keyword required to get results. In both cases the code is nested in a `Function()` call, so the scope is always global.
* Syntax highlighting option in the context menu (depends on the [highlight](https://www.andre-simon.de/doku/highlight/highlight.html) program).

### Output

* Click on an object result to inspect it.
* Line wrapping option in the context menu.

### Inspection

* Filter options in the context menu of the column headers.
* Property value/getter/setter by hovering on the `Type` column.

## Under the hood:

* New Gtk4 `ColumnView` widget and `TreeListModel`.
* New `JSObject` GJS-GObject type.
* New ES module support.
* A dynamic language: JavaScript.
