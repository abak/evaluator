/*
 * Use the evaluator as an inspector page, from an ES module.
 * command: gjs -m inspector.mjs
 */

import Gtk from 'gi://Gtk?version=4.0';
import Evaluator from '../evaluator/evaluator.js';

let application = new Gtk.Application();

application.connect('activate', () => {
    let window = new Gtk.Window({
        application,
        title: "Evaluator as an inspector page",
        defaultWidth: 300, defaultHeight: 150,
        child: new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            halign: Gtk.Align.CENTER, valign: Gtk.Align.CENTER,
            spacing: 12,
        }),
    });

    let button = new Gtk.Button({ label: "Open Inspector" });
    button.connect('clicked', () => Gtk.Window.set_interactive_debugging(true));
    window.child.append(button);

    window.present();

    let evaluator = new Evaluator({
        context: {
            myBox: window.child,
        },
    });

    Evaluator.addToInspector(evaluator);
});

application.run([]);
