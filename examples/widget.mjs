/*
 * Use the evaluator as a window widget, from an ES module.
 * command: gjs -m widget.mjs
 */

import Gtk from 'gi://Gtk?version=4.0';
import Evaluator from '../evaluator/evaluator.js';

let application = new Gtk.Application();

application.connect('activate', () => {
    let evaluator = new Evaluator({
        context: application,
    });

    new Gtk.Window({
        application, title: "Evaluator as a window widget",
        defaultWidth: 894, defaultHeight: 512,
        child: evaluator,
        visible: true,
    });
});

application.run([]);
