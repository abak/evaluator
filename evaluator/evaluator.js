/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { GLib, GObject, Gtk } from './gi.js';
import { InputBuffer, OutputBuffer } from './buffers.js';
import { HeaderModel, ObjectTreeFilterModel } from './models.js';
import { MainPaned } from './views.js';

const CONSTANTES = [
    ['Gdk', 'imports.gi.Gdk'],
    ['Gio', 'imports.gi.Gio'],
    ['GLib', 'imports.gi.GLib'],
    ['GObject', 'imports.gi.GObject'],
    ['Gtk', 'imports.gi.Gtk'],
];

// FIXME: Regexp slashes are not supported.
const getExpression = function(text) {
    // Join multiline expression members.
    text = text.replace(/(\s*\n\s*)(\.|\+|-|\*|\/|%|\||&)/g, '$2');

    let offset = 0;
    let search = [];

    while (offset < text.length) {
        let index = text.length - 1 - offset;
        let char = text.charAt(index);

        if (text.charAt(index - 1) == '\\' && text.charAt(index - 2) != '\\') {
            offset++;
            continue;
        }

        if (char == search[search.length - 1]) {
            search.pop();
            offset++;
            continue;
        }

        if (["'", '"', '`'].includes(search[search.length - 1])) {
            offset++;
            continue;
        }

        if (char == ')')
            search.push('(');
        if (char == ']')
            search.push('[');
        if (["'", '"', '`'].includes(char))
            search.push(char);

        if (!search.length && !char.match(/[\w.)\]]/))
            break;

        offset++;
    }

    return text.slice(text.length - offset);
};

const getCommonPrefix = function(suggestions) {
    let prefix = '';

    for (let i = 0; i < suggestions[0].length; i++) {
        if (suggestions.some(suggestion => suggestion.charAt(i) != suggestions[0].charAt(i)))
            break;

        prefix += suggestions[0].charAt(i);
    }

    return prefix;
};

const Evaluator = GObject.registerClass({
    GTypeName: 'Evaluator',
    Properties: {
        'context': GObject.ParamSpec.jsobject(
            'context', "Context", "The 'this' property of the execution context of the code evaluation",
            GObject.ParamFlags.READWRITE,
        ),
    },
}, class Evaluator extends MainPaned {
    static addToInspector(instance) {
        Gtk.Window.get_toplevels().connect('items-changed', (list, position, removed_, added) => {
            for (let i = 0; i <= added - 1; i++) {
                let window = list.get_item(position + i);

                if (!window.toString().includes('GtkInspectorWindow'))
                    continue;

                let stack = window.child;
                stack.add_child(instance).set_title("Evaluator");
                break;
            }
        });
    }

    _init(params) {
        super._init(Object.assign({
            context: {},
            headerModel: new HeaderModel(),
            inputBuffer: new InputBuffer(),
            outputBuffer: new OutputBuffer(),
            objectTreeFilterModel: new ObjectTreeFilterModel(),
        }, params));

        CONSTANTES.forEach(([name, value]) => this.headerModel.add(name, value));

        this.context[Symbol.for('evaluator-toString')] = function() {
            return 'this';
        };

        this._results = [];
        globalThis.r = index => this._results[index];

        this._history = [];
        this._history.current = 0;
        this._history.popCurrent = function() {
            return this.splice(this.current, 1)[0];
        };
        this._history.pushCurrent = function(item) {
            this.splice(this.current, 0, item);
        };

        this.objectTreeFilterModel.root = this.context;
        this.objectTreeFilterModel.connect('print', (model_, { name, value }) => {
            this.print(name, value);
        });

        this.outputBuffer.connect('result-activated', (buffer_, index) => {
            this.objectTreeFilterModel.root = this._results[index];
        });

        ['backward', 'forward', 'up', 'down', 'evaluate', 'complete'].forEach(id => {
            this.inputBuffer.connect(id, (buffer_, ...args) => this[id](...args));
        });
    }

    // Initialize the output with the context.
    vfunc_map() {
        super.vfunc_map();

        if (this.outputBuffer.get_char_count())
            return;

        GLib.timeout_add(GLib.PRIORITY_DEFAULT, 100, () => {
            this.inputBuffer.setText("this");
            this.inputBuffer.activate();

            return GLib.SOURCE_REMOVE;
        });
    }

    complete(text) {
        text = getExpression(text);

        let base, prop;
        let suggestions = [];

        if (text == '' || text.match(/^(\w*)$/)) {
            [base, prop] = ['globalThis', text];
            suggestions = suggestions.concat('this', [...this.headerModel].map(constante => constante.name));
        } else if (text.match(/(.*)\.(.*)/)) {
            [, base, prop] = text.match(/(.*)\.(.*)/);
        }

        let header = '"use strict";'.concat(...this.headerModel, '\n');
        let command = header.concat(`return ${base}`);
        let object;

        try {
            object = Function(command).bind(this.context)();
        } catch(e) {}

        while (object) {
            let propertyNames = Object.getOwnPropertyNames(object).filter(name => !suggestions.includes(name));
            suggestions = suggestions.concat(propertyNames);
            object = Object.getPrototypeOf(object);
        }

        suggestions = suggestions
            .filter(name => name.startsWith(prop))
            .sort((suggestion1, suggestion2) => suggestion1[Symbol.for('evaluator-compare')](suggestion2));

        this.outputBuffer.deleteSuggestion();

        if (suggestions.length == 1)
            this.inputBuffer.insert_at_cursor(suggestions[0].slice(prop.length), -1);

        if (suggestions.length > 1) {
            let suggestionText = suggestions.reduce((accumulator, suggestion, index) => {
                return accumulator + suggestion + (index % 5 == 4 ? ',\n' : ', ');
            }, '');

            this.outputBuffer.pushSuggestion(suggestionText);

            let prefix = getCommonPrefix(suggestions);
            this.inputBuffer.insert_at_cursor(prefix.slice(prop.length), -1);
        }
    }

    evaluate(text) {
        this._history.current = this._history.length;
        this._history.pushCurrent(text);
        this._history.current += 1;

        text = text.trim();

        this.outputBuffer.pushCode(text);

        let header = '"use strict";'.concat(...this.headerModel, '\n');
        let evalFunction = this.inputBuffer.asFunction ?
            Function(header.concat(text)).bind(this.context) :
            Function('command', 'let arguments = undefined; return eval(command);')
                .bind(this.context, header.concat('let command = undefined;', text));
        let result, tag;

        try {
            result = evalFunction();

            if (result == "use strict")
                result = undefined;

            tag = result == undefined ? 'warning' : 'success';
        } catch(e) {
            result = e;
            tag = 'error';
        }

        this._results.push(result);
        let resultText = typeof result == 'string' ? `"${result}"` : String(result);
        let isClickable = result?.[Symbol.for('evaluator-isInspectable')]?.();

        this.outputBuffer.pushResult(resultText, tag, isClickable, this._results.length - 1);
    }

    print(name, value) {
        this.outputBuffer.pushCode(name);

        this._results.push(value);
        let tag = value == undefined ? 'warning' : 'success';
        let valueText = typeof value == 'string' ? `"${value}"` : String(value);
        let isClickable = value?.[Symbol.for('evaluator-isInspectable')]?.();

        this.outputBuffer.pushResult(valueText, tag, isClickable, this._results.length - 1);
    }

    backward() {
        if (this._history.current == 0)
            return;

        let text = this.inputBuffer.getText();

        if (text.trim())
            this._history.pushCurrent(text);

        this._history.current -= 1;
        this.inputBuffer.setText(this._history.popCurrent() || '');
    }

    forward() {
        if (this._history.current > this._history.length)
            return;

        let text = this.inputBuffer.getText();

        if (text.trim())
            this._history.pushCurrent(text);
        else if (this._history.current == this._history.length)
            return;

        this._history.current += 1;
        this.inputBuffer.setText(this._history.popCurrent() || '');
    }

    up(index) {
        if (index == -1)
            index = this._results.length;

        if (--index >= 0 && index < this._results.length)
            this.inputBuffer.setResult(index);
    }

    down(index) {
        if (++index >= 0 && index < this._results.length)
            this.inputBuffer.setResult(index);
        else
            this.inputBuffer.setText('');
    }
});

export default Evaluator;
