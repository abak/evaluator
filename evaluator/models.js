/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { Gio, GObject, Gtk } from './gi.js';

Object.prototype[Symbol.for('evaluator-isInspectable')] = function() {
    return (typeof this == 'object' || typeof this == 'function')
        && !this.toString()?.includes('GjsFileImporter');
};

String.prototype[Symbol.for('evaluator-compare')] = function(compareString) {
    if (this.startsWith('_') && compareString.startsWith('_'))
        return this.slice(1)[Symbol.for('evaluator-compare')](compareString.slice(1));

    if (!this.startsWith('_') && compareString.startsWith('_'))
        return -1;

    if (this.startsWith('_'))
        return 1;

    return this.localeCompare(compareString);
};

const getPrototypeName = function(object) {
    if (object.constructor.$gtype?.name && object.constructor.$gtype.name != "JSObject")
        return `${object.constructor.$gtype.name}.prototype`;

    if (object.constructor.name)
        return `${object.constructor.name}.prototype`;

    return String(object.constructor.prototype).split('\n')[0];
};

const getInstanceName = function(object) {
    if (object.constructor.$gtype?.name && object.constructor.$gtype.name != "JSObject")
        return `${object.constructor.$gtype.name} instance`;

    if (object.constructor.name)
        return `${object.constructor.name} instance`;

    return String(object).split('\n')[0];
};

Object.prototype[Symbol.for('evaluator-toString')] = function() {
    if (String(this).includes('GjsFileImporter'))
        return String(this);

    if (this.$gtype?.name && this.$gtype.name != "JSObject")
        return this.$gtype.name;

    if (typeof this == 'function' && this.name)
        return this.name;

    if (this.constructor.prototype == this)
        return getPrototypeName(this);
    else
        return getInstanceName(this);
};

const ConstanteItem = GObject.registerClass({
    GTypeName: 'EvaluatorConstanteItem',
    Properties: {
        'name': GObject.ParamSpec.string(
            'name', "Name", "The constante name",
            GObject.ParamFlags.READWRITE, null
        ),
        'value': GObject.ParamSpec.string(
            'value', "Value", "The constante value, as a string to evaluate",
            GObject.ParamFlags.READWRITE, null
        ),
    },
}, class extends GObject.Object {
    equal(constante) {
        return (constante.name == this.name) && (constante.value == this.value);
    }

    toString() {
        return `const ${this.name} = ${this.value};`;
    }
});

export const HeaderModel = GObject.registerClass({
    GTypeName: 'EvaluatorHeaderModel',
    Implements: [Gio.ListModel],
}, class extends GObject.Object {
    _init() {
        super._init();
        this._constantes = [];
    }

    vfunc_get_item_type() {
        return ConstanteItem.$gtype;
    }

    vfunc_get_item(position) {
        return this._constantes[position];
    }

    vfunc_get_n_items() {
        return this._constantes.length;
    }

    [Symbol.iterator]() {
        return this._constantes[Symbol.iterator]();
    }

    add(name, value) {
        if (!name.trim() || !value.trim())
            return;

        let newConstante = new ConstanteItem({ name, value });

        let sameNameIndex = this._constantes.findIndex(constante => constante.name == newConstante.name);
        if (sameNameIndex != -1) {
            this._constantes[sameNameIndex] = newConstante;
            this.items_changed(sameNameIndex, 1, 1);
            return;
        }

        this._constantes.push(newConstante);
        this.items_changed(this._constantes.length - 1, 0, 1);
    }

    remove(name) {
        let index = this._constantes.findIndex(constante => constante.name == name);
        if (index != -1) {
            this._constantes.splice(index, 1);
            this.items_changed(index, 1, 0);
        }
    }
});

const PropertyItem = GObject.registerClass({
    GTypeName: 'EvaluatorPropertytItem',
}, class extends GObject.Object {
    _init(name, owner, chain, descriptor) {
        Object.assign(this, { name, owner, chain }, descriptor);

        if (this.hasOwnProperty('value'))
            this.type = typeof this.value;
        else if (this.hasOwnProperty('get') && this.hasOwnProperty('set'))
            this.type = 'get/set';
        else if (this.hasOwnProperty('get'))
            this.type = 'getter';
        else if (this.hasOwnProperty('set'))
            this.type = 'setter';

        ['value', 'get', 'set'].forEach(key => {
            if (this.hasOwnProperty(key))
                try {
                    this[key] = String(this[key]);
                } catch(e) {
                    this[key] = '';
                }
        });

        super._init();
    }

    resolve(root) {
        let name = root[Symbol.for('evaluator-toString')]();
        let value = root;

        if (name.includes(' '))
            name = `[${name}]`;

        this.chain.concat([this.name]).forEach(key => {
            name += `.${key}`;
            value = value[key];
        });

        return { name, value };
    }

    get details() {
        let details = "";

        ['value', 'get', 'set'].forEach(key => {
            if (this.hasOwnProperty(key)) {
                let value = String(this[key]);

                if (value.length > 300)
                    value = value.slice(0, 300) + '…';

                details += `${key}: ${value}\n\n`;
            }
        });

        return details.trim();
    }
});

/*
 * Get a flat list of property items:
 * 1. grouped by prototype ('owner')
 * 2. sorted by name for the same prototype
 *
 * The GObject object prototype chain is a bit flattened to avoid duplicates.
 *
 * The model and its items do not hold references to objects and their prototype chain objects, but only strings.
 * Property values can be retrieved by passing the root object to the 'resolve' method of the items.
 */
const ObjectModel = GObject.registerClass({
    GTypeName: 'EvaluatorObjectModel',
    Implements: [Gio.ListModel],
}, class extends GObject.Object {
    static createModelFunc(root, propertyItem) {
        if (!propertyItem.hasOwnProperty('value'))
            return null;

        let object = propertyItem.resolve(root).value;

        if (!object?.[Symbol.for('evaluator-isInspectable')]?.())
            return null;

        let chain = Array.from(propertyItem.chain);
        chain.push(propertyItem.name);

        return new ObjectModel(object, chain);
    }

    _init(object, chain = []) {
        super._init();

        if (!object?.[Symbol.for('evaluator-isInspectable')]?.()) {
            this._properties = [];
            return;
        }

        let ownersMap = new Map();

        for (let proto = object; proto; proto = Object.getPrototypeOf(proto)) {
            let owner = proto == object ? proto[Symbol.for('evaluator-toString')]() : getPrototypeName(proto);
            let propertiesMap = ownersMap.get(owner) || new Map();

            let descriptors = {};
            try {
                descriptors = Object.getOwnPropertyDescriptors(proto);
            } catch(e) {
                // proto may be a GIRepositoryNamespace. Ignore descriptors whose value cannot be defined.
                Object.getOwnPropertyNames(proto).forEach(propertyName => {
                    try {
                        descriptors[propertyName] = Object.getOwnPropertyDescriptor(proto, propertyName);
                    } catch(e) {}
                });
            }

            Object.entries(descriptors).forEach(([key, descriptor]) => {
                // Assign the old descriptor upon the new descriptor.
                if (propertiesMap.has(key))
                    descriptor = Object.assign(descriptor, propertiesMap.get(key));

                propertiesMap.set(key, descriptor);
            });

            ownersMap.set(owner, propertiesMap);
        }

        this._properties = [].concat(...[...ownersMap.entries()].map(([owner, propertiesMap]) => {
            return [...propertiesMap.entries()]
                .map(([key, descriptor]) => new PropertyItem(String(key), owner, chain, descriptor))
                .sort((item1, item2) => item1.name[Symbol.for('evaluator-compare')](item2.name));
        }));
    }

    vfunc_get_item_type() {
        return PropertyItem.$gtype;
    }

    vfunc_get_item(position) {
        return this._properties[position];
    }

    vfunc_get_n_items() {
        return this._properties.length;
    }
});

// (SingleSelection in views.js) < FilterListModel < TreeListModel < ObjectModel < PropertyItem
export const ObjectTreeFilterModel = GObject.registerClass({
    GTypeName: 'EvaluatorObjectTreeModel',
    Properties: {
        'root': GObject.ParamSpec.jsobject(
            'root', "Root", "The root object of the tree",
            GObject.ParamFlags.READWRITE,
        ),
        'filter-name': GObject.ParamSpec.boolean(
            'filter-name', "Filter name", "Whether to hide the private properties",
            GObject.ParamFlags.READWRITE, false
        ),
        'filter-type': GObject.ParamSpec.boolean(
            'filter-type', "Filter type", "Whether to hide the functions",
            GObject.ParamFlags.READWRITE, false
        ),
        'filter-owner': GObject.ParamSpec.boolean(
            'filter-owner', "Filter owner", "Whether to hide the base prototypes (Object and GObject)",
            GObject.ParamFlags.READWRITE, false
        ),
        'filter-enumerable': GObject.ParamSpec.boolean(
            'filter-enumerable', "Filter enumerable", "Whether to filter the enumerable properties",
            GObject.ParamFlags.READWRITE, false
        ),
        'filter-writable': GObject.ParamSpec.boolean(
            'filter-writable', "Filter writable", "Whether to filter the writable properties",
            GObject.ParamFlags.READWRITE, false
        ),
        'filter-configurable': GObject.ParamSpec.boolean(
            'filter-configurable', "Filter configurable", "Whether to filter the configurable properties",
            GObject.ParamFlags.READWRITE, false
        ),
    },
    Signals: {
        'print': { param_types: [GObject.TYPE_JSOBJECT] },
    },
}, class extends Gtk.FilterListModel {
    _init(params) {
        super._init(params);

        this.filter = new Gtk.CustomFilter();
        this.filter.set_filter_func(this._onItemFiltered.bind(this));
    }

    _onItemFiltered(item) {
        return (!this.filterName || !item.item.name.startsWith('_'))
            && (!this.filterType || item.item.type != 'function')
            && (!this.filterOwner || (item.item.owner != 'Object.prototype' && item.item.owner != 'GObject.prototype'))
            && (!this.filterEnumerable || item.item.enumerable)
            && (!this.filterWritable || item.item.writable)
            && (!this.filterConfigurable || item.item.configurable);
    }

    on_notify(pspec) {
        if (pspec.get_name().startsWith('filter-'))
            this.filter.changed(this[pspec.get_name()] ? Gtk.FilterChange.MORE : Gtk.FilterChange.LESS);
    }

    get root() {
        return this._root || null;
    }

    set root(root) {
        this._root = root;
        this.notify('root');

        this.refresh();
    }

    refresh() {
        if (this.root)
            this.model = Gtk.TreeListModel.new(
                new ObjectModel(this.root),
                false, false,
                ObjectModel.createModelFunc.bind(ObjectModel, this.root)
            );
        else
            this.model = null;
    }

    activate(position) {
        let propertyItem = this.model.get_item(position).item;
        this.emit('print', propertyItem.resolve(this.root));
    }
});
