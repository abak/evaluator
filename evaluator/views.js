/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { Gdk, Gio, GLib, GObject, Gtk, Pango } from './gi.js';
import gettext from 'gettext';

const _ = gettext.domain('gtk40').gettext;

const CSS = `
#evaluator #objectbar label,
#evaluator #inputbar > revealer > box { border-top-width: 0; }
#evaluator #inputbar #chevron,
#evaluator #inputbar expander-widget title { opacity: 0.55; text-shadow: none; font-weight: bold; }
#evaluator #objectbar button,
#evaluator #inputbar #header > menubutton > button,
#evaluator #inputbar #actionbox button:not(.suggested-action) { opacity: 0.55; text-shadow: none; }
#evaluator #objectbar button,
#evaluator #inputbar #actionbox button { -gtk-icon-size: 16px; min-width: 16px; min-height: 16px; }
#evaluator #header > menubutton > button { padding: 0 5px; -gtk-icon-size: 8px; min-height: 20px; }
#evaluator #header > menubutton > popover box { border-spacing: 4px 4px;}
#evaluator #inputbar #actionbox { border-spacing: 4px 4px; }
#evaluator #input { padding: 8px; margin: 0px 6px 6px 6px; }
#evaluator #input, #output { font-size: smaller; }
#evaluator #output { color: alpha(@theme_text_color, 0.65); }
#evaluator #output #gutter:disabled label { color: alpha(@theme_fg_color, 0.65); }
`;

const HeaderView = GObject.registerClass({
    GTypeName: 'EvaluatorHeaderView',
    Properties: {
        'model': GObject.ParamSpec.object(
            'model', "Model", "The model that is displayed by the view",
            GObject.ParamFlags.READWRITE, Gio.ListModel.$gtype
        ),
    },
}, class extends Gtk.Box {
    _init(params) {
        super._init(params);

        let button = new Gtk.MenuButton({ label: '+', halign: Gtk.Align.CENTER });
        button.get_first_child().add_css_class('flat');
        button.get_first_child().child.get_last_child().hide();

        button.set_create_popup_func(button => {
            let nameEntry = new Gtk.Entry({ placeholderText: "Joe" });
            let valueEntry = new Gtk.Entry({ placeholderText: "imports.gi.Joe" });
            let addButton = new Gtk.Button({ label: _("Add") });
            addButton.connect('clicked', () => {
                this.model.add(nameEntry.text, valueEntry.text);
                button.popdown();
            });
            let child = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
            child.append(nameEntry);
            child.append(valueEntry);
            child.append(addButton);
            button.popover = new Gtk.Popover({ child });
        });

        this.append(button);
    }

    _onItemsChanged(model, position, removed, added) {
        let children = [...this];

        children.slice(position, position + removed).forEach(this.remove.bind(this));

        let sibling = children[position - 1] || null;
        for (let i = position; i < position + added; i++) {
            let item = this.model.get_item(i);

            let button = new Gtk.MenuButton({ label: item.name, halign: Gtk.Align.CENTER });
            button.get_first_child().add_css_class('flat');
            button.get_first_child().child.get_last_child().hide();

            button.set_create_popup_func(button => {
                let entry = new Gtk.Entry({ text: item.value });
                entry.bind_property('text', item, 'value', GObject.BindingFlags.DEFAULT);
                let removeButton = new Gtk.Button({ label: _("Remove") });
                removeButton.connect('clicked', () => this.model.remove(item.name));
                let child = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
                child.append(entry);
                child.append(removeButton);
                button.popover = new Gtk.Popover({ child });
            });

            this.insert_child_after(button, sibling);
            sibling = button;
        }
    }

    // The menu button is removed from its popover.
    remove(child) {
        child.popdown();
        super.remove(child);
    }

    // Do not count the last child ('child && child.get_next_sibling()')
    *[Symbol.iterator]() {
        for (let child = this.get_first_child(); child && child.get_next_sibling(); child = child.get_next_sibling())
            yield child;
    }

    get model() {
        return this._model || null;
    }

    set model(model) {
        if (this.model == model)
            return;

        if (this.model) {
            this.model.disconnect(this._itemsChangedHandler);
            [...this].forEach(child => this.remove(child));
        }

        this._model = model;
        this.notify('model');

        if (model) {
            this._itemsChangedHandler = model.connect('items-changed', this._onItemsChanged.bind(this));
            this._onItemsChanged(model, 0, 0, model.get_n_items());
        }
    }
});

const InputView = GObject.registerClass({
    GTypeName: 'EvaluatorInputView',
    Properties: {
        'enter-activates': GObject.ParamSpec.boolean(
            'enter-activates', "Enter activates", "Pressing the Enter key activates the entry",
            GObject.ParamFlags.READWRITE, true
        ),
    },
}, class extends Gtk.TextView {
    _init(params = {}) {
        super._init(Object.assign(params, {
            cssName: 'entry',
            monospace: true,
        }));

        let keyController = new Gtk.EventControllerKey();
        keyController.connect('key-pressed', this._onKeyPressed.bind(this));
        this.add_controller(keyController);

        let action, actionGroup, menu;
        this.insert_action_group('misc', actionGroup = new Gio.SimpleActionGroup());
        actionGroup.add_action(action = new Gio.SimpleAction({
            name: 'syntax-highlighting',
            state: GLib.Variant.new_boolean(false)
        }));
        this.buffer.bind_property_full(
            'syntax-highlighting', action, 'state',
            GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.BIDIRECTIONAL,
            (binding_, active) => {
                if (action.state.unpack() == active)
                    return false;

                action.state = GLib.Variant.new_boolean(active);
                return false;
            },
            (binding_, state) => {
                if (this.buffer.syntaxHighlighting == state.unpack())
                    return false;

                if (state.unpack()) {
                    let color = this.get_style_context().get_color();
                    this.buffer.syntaxHighlightingDark = (color.red + color.green + color.blue) > 3 / 2;
                }

                this.buffer.syntaxHighlighting = state.unpack();
                return false;
            }
        );
        this.buffer.bind_property('syntax-highlighting-available', action, 'enabled', GObject.BindingFlags.SYNC_CREATE);
        this.set_extra_menu(menu = new Gio.Menu());
        menu.append("Syntax highlighting", 'misc.syntax-highlighting');

        let color = this.get_style_context().get_color();
        this.buffer.syntaxHighlightingDark = (color.red + color.green + color.blue) > 3 / 2;
    }

    _onKeyPressed(controller, keyval, keycode, modifiers) {
        let enter = keyval == Gdk.KEY_Return || keyval == Gdk.KEY_KP_Enter;
        let backward = keyval == Gdk.KEY_b || keyval == Gdk.KEY_B;
        let forward = keyval == Gdk.KEY_n || keyval == Gdk.KEY_N;
        let up = keyval == Gdk.KEY_Up || keyval == Gdk.KEY_KP_Up;
        let down = keyval == Gdk.KEY_Down || keyval == Gdk.KEY_KP_Down;
        let tab = keyval == Gdk.KEY_Tab || keyval == Gdk.KEY_ISO_Left_Tab;
        let control = modifiers & Gdk.ModifierType.CONTROL_MASK;
        let shift = modifiers & Gdk.ModifierType.SHIFT_MASK;

        if (backward && control)
            this.buffer.emit('backward');
        else if (forward && control)
            this.buffer.emit('forward');
        else if (up)
            return this.buffer.handleUp();
        else if (down)
            return this.buffer.handleDown();
        else if (enter && !control && !shift && this.enterActivates)
            this.buffer.activate();
        else if (tab)
            this.buffer.handleTab(shift);
        else
            return Gdk.EVENT_PROPAGATE;

        return Gdk.EVENT_STOP;
    }
});

const OutputView = GObject.registerClass({
    GTypeName: 'EvaluatorOutputView',
}, class extends Gtk.TextView {
    _init(params = {}) {
        super._init(Object.assign(params, {
            editable: false, cursorVisible: false, monospace: true,
            topMargin: 4, rightMargin: 4, bottomMargin: 4, leftMargin: 4,
            wrapMode: Gtk.WrapMode.WORD_CHAR,
        }));

        this.set_gutter(Gtk.TextWindowType.LEFT, new Gtk.ScrolledWindow({
            vscrollbarPolicy: Gtk.PolicyType.EXTERNAL,
            sensitive: false,
            child: this._fixed = new Gtk.Fixed({
                marginTop: this.topMargin, marginEnd: this.rightMargin,
                marginBottom: this.bottomMargin, marginStart: this.leftMargin,
                name: 'gutter',
            }),
        }));

        this.buffer.connect('invalidate-gutter-marks', () => {
            [...this._fixed].forEach(child => this._fixed.remove(child));
        });

        this.buffer.connect('text-added', () => {
            // timeout: Give to the gutter the time to be synchronized before
            // updating the vertical adjustment.
            GLib.timeout_add(GLib.PRIORITY_DEFAULT, 25, () => {
                this.scroll_mark_onscreen(this.buffer.getLastLineMark());
                return GLib.SOURCE_REMOVE;
            });
        });

        let motionController = new Gtk.EventControllerMotion();
        this.add_controller(motionController);
        motionController.connect('motion', (controller_, x, y) => {
            // The pointer is over the gutter.
            if (x <= this.get_first_child().get_width()) {
                this.set_cursor(null);
                this._hasPointerCursor = false;
                return;
            }

            // Offset the gutter width.
            x -= this.get_first_child().get_width();
            [x, y] = this.window_to_buffer_coords(Gtk.TextWindowType.TEXT, x, y);

            let [hasText, iter] = this.get_iter_at_location(x, y);
            if (hasText && this.buffer.handleMotion(iter)) {
                this.set_cursor_from_name('pointer');
                this._hasPointerCursor = true;
            } else if (this._hasPointerCursor) {
                this.set_cursor_from_name('text');
                this._hasPointerCursor = false;
            }
        });

        let clickController = new Gtk.GestureClick();
        this.add_controller(clickController);
        clickController.connect('released', () => {
            if (this._hasPointerCursor)
                this.buffer.handleClick();
        });

        let action, actionGroup, menu;
        let state = GLib.Variant.new_boolean(this.wrapMode != Gtk.WrapMode.NONE);
        this.insert_action_group('misc', actionGroup = new Gio.SimpleActionGroup());
        actionGroup.add_action(action = new Gio.SimpleAction({ name: 'wrap', state }));
        action.connect('change-state', (action, state) => {
            action.set_state(state);
            this.wrapMode = Gtk.WrapMode[state.unpack() ? 'WORD_CHAR' : 'NONE'];
        });
        this.set_extra_menu(menu = new Gio.Menu());
        menu.append("Wrap lines", 'misc.wrap');
    }

    vfunc_map() {
        this.vadjustment.bind_property('value', this._fixed.parent.vadjustment, 'value', GObject.BindingFlags.DEFAULT);

        this.vadjustment.connect('notify::upper', () => {
            GLib.idle_add(GLib.PRIORITY_DEFAULT_IDLE, () => {
                if (this._isSyncing)
                    return;

                this._isSyncing = true;
                this._syncGutter();
                this._isSyncing = false;
            });
        });

        super.vfunc_map();
    }

    vfunc_css_changed(...args) {
        super.vfunc_css_changed(...args);

        // Gtk bug workaround: the method is called on all TextView instances.
        if (this.constructor != OutputView)
            return;

        let errorRgba = this.get_style_context().lookup_color('error_color')[1];
        let successRgba = this.get_style_context().lookup_color('success_color')[1];
        let warningRgba = this.get_style_context().lookup_color('warning_color')[1];

        if (this.buffer.tagTable.lookup('error')) {
            this.buffer.tagTable.lookup('error').foregroundRgba = errorRgba;
            this.buffer.tagTable.lookup('success').foregroundRgba = successRgba;
            this.buffer.tagTable.lookup('warning').foregroundRgba = warningRgba;
            return;
        }

        this.buffer.tagTable.add(new Gtk.TextTag({ name: 'error', foregroundRgba: errorRgba }));
        this.buffer.tagTable.add(new Gtk.TextTag({ name: 'success', foregroundRgba: successRgba }));
        this.buffer.tagTable.add(new Gtk.TextTag({ name: 'warning', foregroundRgba: warningRgba }));
        this.buffer.tagTable.add(new Gtk.TextTag({ name: 'below', pixelsBelowLines: 4 }));
    }

    _syncGutter() {
        let endWidget = this._fixed.get_first_child();
        if (!endWidget)
            this._fixed.put(endWidget = new Gtk.Label(), 0, 0);

        let endRect =  this.get_iter_location(this.buffer.get_end_iter());
        if (this._fixed.get_child_position(endWidget)[1] == endRect.y)
            return;
        this._fixed.move(endWidget, 0, endRect.y);

        let marks = this.buffer.gutterMarks;
        let widgets = [...this._fixed].filter(widget => widget != endWidget);

        for (let i = widgets.length - 1; i >= 0; i--) {
            let iter = this.buffer.get_iter_at_mark(marks[i]);
            let rect = this.get_iter_location(iter);

            if (this._fixed.get_child_position(widgets[i])[1] == rect.y)
                break;

            this._fixed.move(widgets[i], 0, rect.y);
        }

        for (let j = widgets.length; j < marks.length; j++) {
            let iter = this.buffer.get_iter_at_mark(marks[j]);
            let rect = this.get_iter_location(iter);
            let widget = new Gtk.Label({ label: `<tt>${marks[j].text.padStart(4, ' ')}</tt>`, useMarkup: true });
            this._fixed.put(widget, 0, rect.y);
        }
    }
});

const ObjectFactory = GObject.registerClass({
    GTypeName: 'EvaluatorObjectFactory',
    Properties: {
        'attribute': GObject.ParamSpec.string(
            'attribute', "Attribute", "The property descriptor attribute to display",
            GObject.ParamFlags.READWRITE, ''
        ),
        'has-expander': GObject.ParamSpec.boolean(
            'has-expander', "Has expander", "Whether the row has an expander",
            GObject.ParamFlags.READWRITE, false
        ),
    },
}, class extends Gtk.SignalListItemFactory {
    on_setup(item) {
        let child = new Gtk.Label({ xalign: 0 });

        if (this.attribute == 'name')
            Object.assign(child, { ellipsize: Pango.EllipsizeMode.END, widthChars: 18 });

        item.child = this.hasExpander ? new Gtk.TreeExpander({ child }) : child;
    }

    on_bind(item) {
        let propertyItem = item.item.item;
        let child = this.hasExpander ? item.child.child : item.child;

        child.label = String(propertyItem[this.attribute]);

        if (this.attribute == 'type') {
            let details = propertyItem.details.replace(/^value/, _("Value"));

            try {
                Pango.parse_markup(`<tt><small>${details}</small></tt>`, -1, '0');
                child.tooltipText = null;
                child.tooltipMarkup = `<tt><small>${details}</small></tt>`;
            } catch(e) {
                child.tooltipMarkup = null;
                child.tooltipText = details;
            }
        }

        if (this.hasExpander)
            item.child.listRow = item.item;
    }
});

const ObjectView = GObject.registerClass({
    GTypeName: 'EvaluatorObjectView',
}, class extends Gtk.ColumnView {
    _init(params) {
        super._init(params);
        this.add_css_class('data-table');

        let action, actionGroup = new Gio.SimpleActionGroup();
        this.insert_action_group('object', actionGroup);

        ['name', 'type', 'owner', 'enumerable', 'writable', 'configurable'].forEach(name => {
            action = new Gio.PropertyAction({ name: `filter-${name}`, object: this.model.model, propertyName: `filter-${name}` });
            actionGroup.add_action(action);
        });

        this._append(_("Name"), 'name', "Hide private properties", true);
        this._append(_("Type"), 'type', "Hide functions");
        this._append(_("Defined At"), 'owner', "Hide base prototypes");
        this._append(_("Enumerable"), 'enumerable', "Filter enumerable");
        this._append(_("Writable"), 'writable', "Filter writable");
        this._append(_("Configurable"), 'configurable', "Filter configurable");
    }

    _append(title, attribute, menuLabel, hasExpander = false) {
        let headerMenu = new Gio.Menu();
        headerMenu.append(menuLabel, `object.filter-${attribute}`);

        let factory = new ObjectFactory({ attribute, hasExpander });
        let column = new Gtk.ColumnViewColumn({ title, factory, headerMenu, expand: true, resizable: true });
        this.append_column(column);
    }
});

// Assert the child has no minimum size.
// So the action bar size has no influence on the separator position of the paned.
const ZeroMinBinLayout = GObject.registerClass({
    GTypeName: 'EvaluatorZeroMinBinLayout',
}, class extends Gtk.BinLayout {
    vfunc_measure(widget, orientation, forSize) {
        let [min, nat, minBaseline, natBaseline] = super.vfunc_measure(widget, orientation, forSize);
        widget[orientation == Gtk.Orientation.HORIZONTAL ? 'realMinWidth' : 'realMinHeight'] = min;

        return [0, nat, minBaseline || -1, natBaseline || -1];
    }

    vfunc_allocate(widget, width, height, baseline) {
        super.vfunc_allocate(widget, Math.max(widget.realMinWidth, width), Math.max(widget.realMinHeight, height), baseline || -1);
    }
});

export const MainPaned = GObject.registerClass({
    GTypeName: 'EvaluatorMainPaned',
    Properties: {
        'header-model': GObject.ParamSpec.object(
            'header-model', "Header model", "The header model",
            GObject.ParamFlags.READWRITE, Gio.ListModel.$gtype
        ),
        'input-buffer': GObject.ParamSpec.object(
            'input-buffer', "Input buffer", "The input buffer",
            GObject.ParamFlags.READWRITE, Gtk.TextBuffer.$gtype
        ),
        'output-buffer': GObject.ParamSpec.object(
            'output-buffer', "Output buffer", "The output buffer",
            GObject.ParamFlags.READWRITE, Gtk.TextBuffer.$gtype
        ),
        'object-tree-filter-model': GObject.ParamSpec.object(
            'object-tree-filter-model', "Object tree model", "The object tree model",
            GObject.ParamFlags.READWRITE, Gtk.FilterListModel.$gtype
        ),
    },
}, class extends Gtk.Paned {
    _init(params = {}) {
        super._init(Object.assign({
            orientation: Gtk.Orientation.VERTICAL,
            shrinkStartChild: false, shrinkEndChild: false,
            wideHandle: true,
            name: 'evaluator',
        }, params));

        // Define an initial position ratio (0.65).
        let positionHandler = this.connect('notify::max-position', () => {
            this.disconnect(positionHandler);
            this.position = Math.floor(0.65 * this.maxPosition);
        });

        let subPanned = new Gtk.Paned({
            shrinkStartChild: false, shrinkEndChild: false,
            wideHandle: true,
        });

        // XXX: Prevent the paned from trembling when hovering a scrollbar.
        let subPannedPositionHandler = subPanned.connect('notify::position', () => {
            subPanned.disconnect(subPannedPositionHandler);
            subPanned.positionSet = true;
        });

        this.set_start_child(subPanned);

        {
            let outputView = new OutputView({ buffer: this.outputBuffer, name: 'output' });
            subPanned.set_start_child(new Gtk.ScrolledWindow({ child: outputView }));
        }

        {
            let objectView = new ObjectView({ model: new Gtk.SingleSelection({ model: this.objectTreeFilterModel }) });
            objectView.connect('activate', (objectView, position) => {
                // FIXME: The signal event occurs twice.
                // Probably both the column view and the embedded list view emit it.
                if (-objectView._activateTime + (objectView._activateTime = Date.now()) < 50)
                    return;

                this.objectTreeFilterModel.activate(position);
            });

            let objectBar = new Gtk.ActionBar({ name: 'objectbar', layoutManager: new ZeroMinBinLayout() });
            let objectLabel = new Gtk.Label({ ellipsize: 3, hexpand: true, xalign: 0 });
            let objectRefresh = new Gtk.Button({ iconName: 'view-refresh-symbolic', tooltipText: "Refresh", cssClasses: ['image-button', 'flat'] });
            objectBar.set_center_widget(new Gtk.Box({ hexpand: true }));
            objectBar.get_center_widget().append(objectLabel);
            objectBar.get_center_widget().append(objectRefresh);

            this.objectTreeFilterModel.connect('notify::root', () => {
                objectLabel.set_label(`${_("Properties")} (${this.objectTreeFilterModel.root?.[Symbol.for('evaluator-toString')]?.() || ''})`);
                objectView.model.set_selected(0);
            });
            this.objectTreeFilterModel.notify('root');
            objectRefresh.connect('clicked', () => this.objectTreeFilterModel.refresh());

            let objectBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
            objectBox.append(objectBar);
            objectBox.append(new Gtk.ScrolledWindow({ child: objectView, vexpand: true }));

            subPanned.set_end_child(objectBox);
        }

        {
            let inputView = new InputView({ buffer: this.inputBuffer, name: 'input' });

            let chevronlabel = new Gtk.Label({ label: "<tt>>>> JS</tt>", useMarkup: true, name: 'chevron', valign: Gtk.Align.END });
            let headerView = new HeaderView({ model: this.headerModel, name: 'header' });
            let expander = new Gtk.Expander({ label: "Attached constantes", child: headerView });
            expander.get_first_child().orientation = Gtk.Orientation.HORIZONTAL;
            expander.bind_property('expanded', expander.labelWidget, 'visible', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.INVERT_BOOLEAN);

            let backwardButton = new Gtk.Button({ iconName: 'go-previous-symbolic', tooltipText: "Back", cssClasses: ['image-button', 'flat'] });
            let forwardButton = new Gtk.Button({ iconName: 'go-next-symbolic', tooltipText: "Next", cssClasses: ['image-button', 'flat'] });
            let functionButton = new Gtk.ToggleButton({ label: '𝓯', tooltipText: "Evaluate the code as a function", cssClasses: ['image-button', 'flat'] });
            let enterButton = new Gtk.ToggleButton({ label: '⎆', tooltipText: "Enter activates the entry", cssClasses: ['image-button', 'flat'] });
            let evaluateButton = new Gtk.Button({ iconName: 'system-run-symbolic', tooltipText: "Evaluate", cssClasses: ['image-button', 'suggested-action'] });

            backwardButton.connect('clicked', () => this.inputBuffer.emit('backward'));
            forwardButton.connect('clicked', () => this.inputBuffer.emit('forward'));
            this.inputBuffer.bind_property('as-function', functionButton, 'active', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.BIDIRECTIONAL);
            inputView.bind_property('enter-activates', enterButton, 'active', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.BIDIRECTIONAL);
            evaluateButton.connect('clicked', () => this.inputBuffer.activate());

            let actionBox = new Gtk.Box({ valign: Gtk.Align.END, name: 'actionbox' });
            actionBox.append(backwardButton);
            actionBox.append(forwardButton);
            actionBox.append(functionButton);
            actionBox.append(enterButton);
            actionBox.append(evaluateButton);

            let inputBar = new Gtk.ActionBar({ name: 'inputbar', layoutManager: new ZeroMinBinLayout() });
            inputBar.pack_start(chevronlabel);
            inputBar.set_center_widget(expander);
            inputBar.pack_end(actionBox);

            let inputBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
            inputBox.append(inputBar);
            inputBox.append(new Gtk.ScrolledWindow({ child: inputView, vexpand: true }));

            this.set_end_child(inputBox);
            this.set_focus_child(inputBox);
            inputBox.set_focus_child(inputView.parent);
        }
    }

    vfunc_root() {
        super.vfunc_root();

        let cssProvider = new Gtk.CssProvider();
        cssProvider.load_from_data(CSS);
        let display = this.get_style_context().get_display();
        Gtk.StyleContext.add_provider_for_display(display, cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
    }
});
